import Vapor

struct ModelAppFollow: Content {
    var text: String
    var reviews:[ModelAppFollowReview]
}

struct ModelAppFollowAndroid: Content {
    var text: String
}

struct ModelAppFollowReview: Content {
    var review_id: Int
    var version:String
    var store:String
    var rating:Int
    var title:String
    var country:String
    var author:String
    var content:String
    var country_code:String
    var date:String

}

struct ModelDingTalkRequestData: Content {
    var msgtype: String
    var markdown: [String:String]
}

struct ModelDingTalkRequestDataMarkdown: Content {
    var msgtype: String
    var markdown: [String:String]
}

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    
    let dtRobotURL = "https://oapi.dingtalk.com/robot/send?access_token="
    let dtRobotTokeniOS = "f79af9cfb80efd8e6b1841cd26db76b58a1b9996b48270cc20fd31ba95ff5b83"
    let dtRobotTokenAndroid = "3638daaf74ce66899a29ae4e7ec38ae3ef358c1cb1d9a44f178efe485a3d12d5"
    
    // Basic "It works" example
    router.get { req in
        return "It works!"
    }
    
    router.post("lazadaios") { req -> Future<HTTPStatus> in
        return try req.content.decode(ModelAppFollow.self).map(to: HTTPStatus.self) { messageRequest in
            //print("To: \(messageRequest.text)")
            //print("DONE")
            
            let url = dtRobotURL + dtRobotTokeniOS
            let res = try req.client().post(url, headers: ["Content-Type":"application/json"], beforeSend: {post in
                
                //if let review = messageRequest.reviews.first {
                    let body = ModelDingTalkRequestDataMarkdown(msgtype: "markdown",
                                                                markdown: ["title":"AppStore iOS",
                                                                           "text":DingTalkUtils.formatMarkdown(markdown:messageRequest.text)])
                    //let body = ModelDingTalkRequestData(msgtype: "text", text: ["content":DingTalkUtils.formatMessage(review: review)])
                    try post.content.encode(body)
                //}
            })
            print(res)
            
            return .ok
        }
    }
    
    router.post("lazadaandroid") { req -> Future<HTTPStatus> in
        return try req.content.decode(ModelAppFollowAndroid.self).map(to: HTTPStatus.self) { messageRequest in
            
            let url = dtRobotURL + dtRobotTokenAndroid
            let res = try req.client().post(url, headers: ["Content-Type":"application/json"], beforeSend: {post in
                
                let body = ModelDingTalkRequestDataMarkdown(msgtype: "markdown",
                                                            markdown: ["title":"PlayStore Android",
                                                                       "text":DingTalkUtils.formatMarkdown(markdown:messageRequest.text)])
                //let body = ModelDingTalkRequestData(msgtype: "text", text: ["content":messageRequest.text])
                try post.content.encode(body)
            })
            print(res)
            
            return .ok
        }
    }
}

class DingTalkUtils {
    class func flag(country:String) -> String {
        let base : UInt32 = 127397
        var s = ""
        for v in country.unicodeScalars {
            s.unicodeScalars.append(UnicodeScalar(base + v.value)!)
        }
        return String(s)
    }
    
    class func formatMessage(review:ModelAppFollowReview) -> String {
        var rating = ""
        
        for _ in 0..<review.rating {
            rating.append("⭐️")
        }
        
        //let flag = DingTalkUtils.flag(country: review.country_code.capitalized)
        let message = "\(review.store) \(review.version) \(rating)\n[\(review.author)] customer from \(review.country)\n\(review.title)\n\(review.content)"
        return message
    }
    
    class func formatMarkdown(markdown: String) -> String {
        if let index = markdown.index(of: "[Permalink]") {
            let substring = markdown[..<index]
            return String(substring)
        }
        return markdown
    }
}
